- Run `ng new angularboilerplate` with routing and SASS.

- Enforce double quotemarks in tslint.json file.

- Enforce `*.scss` extensions in schematics in angular.json file.

- Refactorize AppComponent into a new src/app/root folder.

- Run `ng g m core` and make the module for root.

- Run `ng g m ui`.

- Create a `modules` folder.

### DOCUMENTATION

- Run `npm run compodoc` to generate documentation (to do after any code modification and before serving the doc)

- Run `compodoc -s` to serve documentation
